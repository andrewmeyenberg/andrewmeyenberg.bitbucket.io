var classtermproject_1_1MotorDriver =
[
    [ "__init__", "classtermproject_1_1MotorDriver.html#a1ae29e4aeb34002d224ef242e1037830", null ],
    [ "disable", "classtermproject_1_1MotorDriver.html#ae4fa3eee820c9a568f8dc4046bcc41b5", null ],
    [ "enable", "classtermproject_1_1MotorDriver.html#abf6d18f7faffc47b20529b0368672a75", null ],
    [ "set_duty", "classtermproject_1_1MotorDriver.html#a428fa55462c5670ddacf1206e38d6285", null ],
    [ "ch1", "classtermproject_1_1MotorDriver.html#ac703689e44294bead2cc270d39e1d662", null ],
    [ "ch2", "classtermproject_1_1MotorDriver.html#ae69abbb279b417ec0a80572bf9b3b060", null ],
    [ "EN_pin", "classtermproject_1_1MotorDriver.html#a3b9fbbeef3d824f7937330f70db76b01", null ],
    [ "IN1_pin", "classtermproject_1_1MotorDriver.html#a4e5bd67dc65fe2ff7b4be6c86e09fefa", null ],
    [ "IN2_pin", "classtermproject_1_1MotorDriver.html#a3aa9dccf9db6295bae3dd6f9d9216519", null ],
    [ "timer", "classtermproject_1_1MotorDriver.html#a9280e234523fff2265fd9335a59b79b7", null ]
];