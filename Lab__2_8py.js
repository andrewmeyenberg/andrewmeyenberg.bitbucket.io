var Lab__2_8py =
[
    [ "Encoder", "classLab__2_1_1Encoder.html", "classLab__2_1_1Encoder" ],
    [ "enc1", "Lab__2_8py.html#a1c473f1bc867e19ed002bff117674ac5", null ],
    [ "enc2", "Lab__2_8py.html#afc899e628384447ead6fdf0daa978b8b", null ],
    [ "pin_B6", "Lab__2_8py.html#a4e1acc7efaba3d38f284f867462d7434", null ],
    [ "pin_B7", "Lab__2_8py.html#a98f24878bbed72c9fd98133d5e1c6424", null ],
    [ "pin_C6", "Lab__2_8py.html#a58c88afec9f4c4f900b531d50baeaba4", null ],
    [ "pin_C7", "Lab__2_8py.html#ac01fa44abfbfc139d624549314b3d851", null ],
    [ "t4ch1", "Lab__2_8py.html#a47a2fceb38f6b6ac5f4ab99a5c9e8678", null ],
    [ "t4ch2", "Lab__2_8py.html#a47cfaa960671888a882c8e97800a1e60", null ],
    [ "t8ch1", "Lab__2_8py.html#a4ef5480342ae11843619929a2a6378f2", null ],
    [ "t8ch2", "Lab__2_8py.html#a569acfb86173a540f3745fb8b0dcae4b", null ],
    [ "tim4", "Lab__2_8py.html#a1f50878717e7b5ded859e7b1c34ef6dd", null ],
    [ "tim8", "Lab__2_8py.html#aadc87aea89c08d6aced84cd165f227d0", null ]
];