var searchData=
[
  ['sensor_63',['sensor',['../namespaceLab__4.html#a605a86d1966232de37682ba926a7aa6e',1,'Lab_4.sensor()'],['../namespacetermproject.html#afc8335fdd03fcf79a2d6fdeb9b25c44a',1,'termproject.sensor()']]],
  ['set_5fduty_64',['set_duty',['../classLab__1_1_1MotorDriver.html#aac30cb0bd0707fa0e462acb0780d6bff',1,'Lab_1.MotorDriver.set_duty()'],['../classLab__3_1_1MotorDriver.html#abc7d4826f381faf7e1f1ef4bb2f1ed19',1,'Lab_3.MotorDriver.set_duty()'],['../classtermproject_1_1MotorDriver.html#a428fa55462c5670ddacf1206e38d6285',1,'termproject.MotorDriver.set_duty()']]],
  ['set_5fposition_65',['set_position',['../classLab__2_1_1Encoder.html#ab3c0c93deac075bf2a6a7eb3b202cc72',1,'Lab_2.Encoder.set_position()'],['../classLab__3_1_1Encoder.html#a707c2de8dadc061fd12fa8c56702bba5',1,'Lab_3.Encoder.set_position()'],['../classtermproject_1_1Encoder.html#a96e56eda71635da124f6f3f17d4522e7',1,'termproject.Encoder.set_position()']]],
  ['setpoint_66',['setpoint',['../classLab__3_1_1Pcontroller.html#a14dbcc8dbfd8e6b1f1b97f034d2fa3f5',1,'Lab_3.Pcontroller.setpoint()'],['../namespaceLab__3.html#aafa33b38f38b45b189d664cf98b57e7e',1,'Lab_3.setpoint()']]]
];
